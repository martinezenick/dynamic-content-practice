import React from 'react';



const ValidationComponent = (props) => {
    let output = 'String is Just Right'
    if (props.length > 10) {
        output = 'String is too Long'
    }
    else if (props.length <= 2) {
        output = 'String is too Short'
    }
    
    return (
        <div>
            <p>{output}</p>
        </div>
    )
};

export default ValidationComponent;