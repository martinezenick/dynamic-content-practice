import React from 'react';

const charComponent = (props) => {
    const charComponentstyle = {
        display: 'inline-block',
        padding: '16px',
        textAlign: 'center',
        margin: '16px',
        border: '1px solid black'
    }

    return (
        <div
        style={charComponentstyle}
        onClick={props.clicked}>
        
            <p>
                {props.letter}
            </p>
        </div>
    )
};

export default charComponent;