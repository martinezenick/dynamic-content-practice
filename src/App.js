import React, { Component } from 'react';
import './App.css';
import ValidationComponent from './ValidationComponent';
import CharComponent from './CharComponent';

class App extends Component {
  state = {
    input: '',
    length: 0
  };

  stringChangeHandler = (event) => {
    const newString = event.target.value;

    console.log(newString)

    const newLength = newString.length;

    this.setState({
      input: newString,
      length: newLength
    });
  }

  characterDeleteHandler = (index) => {
    const changeString = [...this.state.input.split('')];
    changeString.splice(index,1);
    const updatedString = changeString.join('');
    this.setState({input: updatedString,length: updatedString.length});
  }

  render () {

    const letters = this.state.input.split('').map((ch,index) => {
          return <CharComponent
            letter = {ch}
            clicked = {this.characterDeleteHandler.bind(this,index)}
            key = {index}
          />;
        });
    


    
    return (
      <div className="App">
        <h1>
          Practice Displaying Dynamic Content
        </h1>
        <p>Input a String</p>
        <input 
          type="text"
          value = {this.state.input} 
          onChange = {this.stringChangeHandler}
        />
        <p>
          The length of your string is {this.state.length}!
        </p>
        <ValidationComponent
          length = {this.state.length}
        />
        {letters}
      </div>
    );
  }
}

export default App;
